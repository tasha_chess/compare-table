import React from 'react';
import { Switch, Route } from 'react-router-dom';

import { GreetingPage } from './pages/GreetingPage';
import { ComparePage } from './pages/ComparePage';
import { CompareList } from './pages/CompareList';
import { CreateTablePage } from './pages/CreateTablePage';

import { TablesContextProvider } from './context/TablesContext';

import { ROUTES } from './consts/Routes';

import './assets/styles/styles.scss';

function App() {
  return (
    <TablesContextProvider>
      <Switch>
        <Route exact component={GreetingPage} path={ROUTES.MAIN} />
        <Route exact component={CompareList} path={ROUTES.COMPARE} />
        <Route component={ComparePage} path={`${ROUTES.COMPARE}/:id`} />
        <Route exact component={CreateTablePage} path={`${ROUTES.TABLE}`} />
        <Route component={CreateTablePage} path={`${ROUTES.TABLE}/:id`} />
      </Switch>
    </TablesContextProvider>
  );
}

export default App;
