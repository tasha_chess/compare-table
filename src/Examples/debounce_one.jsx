import React, { useState } from 'react';
import { debounce } from 'lodash';

export const DebounceOne = () => {
  const [state, setState] = useState('');
  const debounceEvent = (...args) => debounce(...args);

  const onChange = (value) => debounceEvent(() => {
    setState(value);

    return debounceEvent.cancel();
  }, 1500);

  return (
    <div>
      <input type="text" onChange={(e) => onChange(e.target.value)} />
      <div>{state}</div>
    </div>
  );
};
