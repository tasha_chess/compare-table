import React from 'react';
import PropTypes from 'prop-types';

import { Menu } from './Menu';

import styles from './styles.module.scss';

export const CommonLayout = ({ children }) => {
  return (
    <div className={styles.commonLayout}>
      <Menu />
      {children}
    </div>
  );
};

CommonLayout.propTypes = {
  children: PropTypes.node,
};
