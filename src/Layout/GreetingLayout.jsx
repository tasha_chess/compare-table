import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

export const GreetingLayout = ({ children }) => {
  return (
    <div className={styles.greetingLayout}>
      {children}
    </div>
  );
};

GreetingLayout.propTypes = {
  children: PropTypes.node,
};
