import React, { useState, useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { EditOutlined } from '@ant-design/icons';

import { CreateTableButton } from '../../components/common/Buttons/CreateTableButton';

import { TablesContext } from '../../context/TablesContext';
import { ITableFullData } from '../../types';
import { ROUTES } from '../../consts/Routes';

import styles from './styles.module.scss';

export const Menu = () => {
  const { tables: { tables } } = useContext(TablesContext);
  const [isShown, toggleCollapsed] = useState(false);

  return (
    <div className={styles.menu}>
      <button type="button" className={styles.button} onClick={() => toggleCollapsed(!isShown)}>
        <span />
      </button>
      {isShown && (
        <>
          <NavLink to={ROUTES.MAIN} className={styles.menuTitle}>Меню</NavLink>
          <div className={styles.items}>
            <CreateTableButton />
            <NavLink to={ROUTES.COMPARE}>Compare</NavLink>
            <div className={styles.tablesList}>
              {tables?.map((table:ITableFullData, index: number) => (
                <NavLink key={table.tableName} to={`${ROUTES.TABLE}/${index}`} className={styles.menuItem}>
                  <span className={styles.itemTitle}>{table.tableName}</span>
                  <span className={styles.pencil}><EditOutlined /></span>
                </NavLink>
              ))}
            </div>
          </div>
        </>
      )}
    </div>
  );
};
