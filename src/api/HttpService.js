import axios from 'axios';

class HttpService {
  static get(url) {
    return HttpService.send('get', url);
  }

  static send(method, url, ...rest) {
    return axios({
      method,
      url,
      ...rest,
    }).then((res) => res.data);
  }
}

export default HttpService;
