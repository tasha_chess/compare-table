import React from 'react';
import { NavLink } from 'react-router-dom';

import { ROUTES } from '../../../../consts/Routes';

import styles from './styles.module.scss';

export const CreateTableButton = () => (
  <NavLink to={ROUTES.TABLE} className={styles.link}>
    <span className={styles.plus}>+</span>
    <span className={styles.title}>Создать таблицу</span>
  </NavLink>
);
