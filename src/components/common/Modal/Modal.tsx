import React from 'react';
import { Modal as ModalAntd } from 'antd';

export const Modal = ({
  children, visible, ...rest
}: any) => {
  return (
    <ModalAntd
      visible={visible}
      {...rest}
    >
      {children}
    </ModalAntd>
  );
};
