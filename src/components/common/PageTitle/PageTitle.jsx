import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.module.scss';

export const PageTitle = ({ children, textAlign }) => {
  return (
    <div className={styles.pageTitle} align={textAlign}>
      {children}
    </div>
  );
};

PageTitle.defaultProps = {
  textAlign: 'left',
};

PageTitle.propTypes = {
  children: PropTypes.node,
  textAlign: PropTypes.string,
};
