import React, { useContext } from 'react';
import { Button, Form } from 'antd';

import { TablesContext } from '../../../context/TablesContext';
import { ACTION_TYPE } from '../../../consts/actions';
import { IColumnInfo } from '../../../types';

import { Input } from '../elements/Input/Input';

import styles from './styles.module.scss';

const { ADD_ROW } = ACTION_TYPE;

export const AddNewRow = ({ columnNames, id, closeModal }: any) => {
  const { Item } = Form;
  const { setTable } = useContext(TablesContext);

  const formItem = (name: string, title: string) => (
    <Item name={name}>
      <label htmlFor={name} className={styles.label}>{title}
        <Input name={name} />
      </label>
    </Item>
  );

  const save = (values: any) => {
    setTable({
      type: ADD_ROW,
      payload: values,
      id,
    });

    closeModal();
  };

  return (
    <Form onFinish={save}>
      {columnNames?.map(({ name, title }: IColumnInfo) => formItem(name, title))}
      <Button htmlType="submit">Save</Button>
    </Form>
  );
};
