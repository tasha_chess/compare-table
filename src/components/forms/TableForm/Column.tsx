import React from 'react';
import { Form } from 'antd';

import { Input } from '../elements/Input';

import styles from './styles.module.scss';

const { Item } = Form;

export const Column = (name: string, index: number): JSX.Element => (
  <div className={styles.columnWrapper} key={`item_${index}`}>
    <Item label="Column name" name={`cname${index}`}>
      <Input placeholder="Column name" />
    </Item>
  </div>
);
