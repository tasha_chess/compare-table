import React, { useEffect } from 'react';
import { Button, Form as AntdForm } from 'antd';

import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';

import { Input } from '../elements/Input';

import styles from './styles.module.scss';

const { Item, List } = AntdForm;

export const Form = ({ initialValues, saveTable, deleteTable }:any) => {
  const [form] = AntdForm.useForm();

  useEffect(() => {
    form.resetFields();
  }, [form, initialValues]);

  return (
    <AntdForm form={form} onFinish={saveTable} initialValues={initialValues}>
      <Item
        label="Table name"
        name="tableName"
        rules={[
          {
            required: true,
            whitespace: true,
            message: 'Please input table name',
          },
        ]}
      >
        <Input placeholder="Enter table name" />
      </Item>
      <List name="columns">
        {(fields, { add, remove }) => {
          return (
            <div>
              {fields.map((field) => (
                <Item
                  key={field.key}
                  className={styles.itemWrap}
                >
                  <Item
                    {...field}
                    rules={[
                      {
                        required: true,
                        whitespace: true,
                        message: 'Please input column name or delete this field.',
                      },
                    ]}
                  >
                    <Input placeholder="Column name" />
                  </Item>
                  <MinusCircleOutlined
                    style={{ margin: '0 8px' }}
                    onClick={() => {
                      remove(field.name);
                    }}
                  />
                </Item>
              ))}
              <Item>
                <Button
                  type="dashed"
                  htmlType="button"
                  onClick={() => {
                    add();
                  }}
                  style={{ width: '60%' }}
                >
                  <PlusOutlined /> Add column
                </Button>
              </Item>
            </div>
          );
        }}
      </List>

      <Item>
        <Button type="primary" htmlType="submit">Save</Button>
        <Button onClick={deleteTable}>Delete table</Button>
      </Item>
    </AntdForm>
  );
};
