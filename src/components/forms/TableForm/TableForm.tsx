import React, { useEffect, useState, useContext } from 'react';

import { Form } from './Form';

import history from '../../../history';
import { TablesContext } from '../../../context/TablesContext';

import { ROUTES } from '../../../consts/Routes';
import { ACTION_TYPE } from '../../../consts/actions';
import { ColumnType } from '../../../types';

import styles from './styles.module.scss';

const { ADD_TABLE, EDIT_TABLE, DELETE_TABLE } = ACTION_TYPE;

type initValuesType = {
  tableName: string,
  columns: [ColumnType]
};

export const TableForm = (props:any) => {
  const { tables: { tables }, setTable } = useContext(TablesContext);
  const [initialValues, setInitialValues] = useState<initValuesType | null>(null);

  useEffect(() => {
    const table = tables[props.id];
    const init: initValuesType | null = table ? {
      tableName: table.tableName,
      columns: table.columns.map((column: ColumnType) => column.name),
    } : null;
    setInitialValues(init);
  }, [tables, props.id]);


  const saveTable = (data:any) => {
    const { tableName, columns } = data;
    const path = props.id ? `${ROUTES.COMPARE}/${props.id}` : ROUTES.COMPARE;

    const action = {
      type: props.id ? EDIT_TABLE : ADD_TABLE,
      id: props.id,
      payload: {
        tableName,
        columns,
      },
    };

    setTable(action);

    history.push(path);
  };

  const deleteTable = () => {
    const action = {
      type: DELETE_TABLE,
      id: props.id,
    };

    setTable(action);

    history.push(ROUTES.COMPARE);
  };

  return (
    <div className={styles.formWrapper}>
      {Form({ initialValues, saveTable, deleteTable })}
    </div>
  );
};
