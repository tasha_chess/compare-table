import React from 'react';
import { Input as InputAntd } from 'antd';

import styles from './styles.module.scss';

export const Input = (props:any) => {
  return <span className={styles.input}><InputAntd {...props} /></span>;
};
