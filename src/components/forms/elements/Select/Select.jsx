import React from 'react';
import { Select as SelectAntd } from 'antd';

export const Select = (props) => {
  return <SelectAntd {...props} />;
};
