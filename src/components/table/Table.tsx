import React from 'react';
import { Table as AntdTable } from 'antd';

import styles from './styles.module.scss';

export const Table = ({ columns, dataSourse }:any) => (
  <div className={styles.tableWrap}>
    <AntdTable
      columns={columns}
      dataSource={dataSourse}
    />
  </div>
);
