import React from 'react';
import { Form } from 'antd';

import { Input } from '../forms/elements/Input';

const { Item } = Form;

export const TableCell = ({ isEditing, content, ...rest }:any) => {
  if (isEditing) {
    return (
      <Item {...rest}>
        <Input />
      </Item>
    );
  }

  return (
    <div>
      {content}
    </div>
  );
};
