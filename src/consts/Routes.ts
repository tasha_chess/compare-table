export const ROUTES = {
  MAIN: '/',
  SIGN_IN: '/sign_in',
  COMPARE: '/compare',
  COMPARE_CREATE: '/compare/create_table',
  TABLE: '/table',
};
