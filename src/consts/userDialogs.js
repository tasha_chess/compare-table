export const GREETING = 'Добро пожаловать!';

export const toAuth = 'Авторизоваться';

export const continueWithoutAuth = 'Продолжить без авторизации';

export const GREETING_TEXT = 'Здесь можно сравнить теплое с мягким и не только';
