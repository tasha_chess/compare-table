import React, { useReducer } from 'react';

import { ACTION_TYPE } from '../consts/actions';
import { ITableAction, ITableFullData } from '../types/table';

interface IState {
  tables: ITableFullData[],
}

const initialState:IState = {
  tables: [],
};

export const TablesContext = React.createContext<IState | any>(initialState);

const {
  ADD_TABLE, DELETE_TABLE, EDIT_TABLE, ADD_ROW,
} = ACTION_TYPE;

const reducer = (state: IState, action: ITableAction) => {
  switch (action.type) {
    case ADD_TABLE: {
      const newTables = [...state.tables, action.payload];
      const newTablesStr = JSON.stringify(newTables);
      const columns = action.payload.columns?.map((name: string, index: number) => ({
        name,
        key: index,
      })) || [];
      localStorage.setItem('tables', newTablesStr);

      return { ...state, tables: [...state.tables, { tableName: action.payload.tableName, columns }] };
    }
    case EDIT_TABLE: {
      const newTables = state.tables.map((table, index) => {
        const columns = action.payload.columns.map((name: string, i:number) => ({
          name,
          key: i,
        }));

        if (parseInt(action.id, 10) === index) {
          return { ...table, tableName: action.payload.tableName, columns };
        }

        return table;
      });
      const newTablesStr = JSON.stringify(newTables);

      localStorage.setItem('tables', newTablesStr);

      return { ...state, tables: newTables };
    }
    case DELETE_TABLE: {
      const newTables = state.tables.filter((item, index) => index !== parseInt(action.id, 10));
      const newTablesStr = JSON.stringify(newTables);

      localStorage.setItem('tables', newTablesStr);

      return { ...state, tables: newTables };
    }
    case ADD_ROW: {
      const newTables = state.tables.map((item, index) => {
        if (index === parseInt(action.id, 10)) {
          const dataArray = Object.assign([], item.data);
          const keys = Object.keys(action.payload);

          const data = keys.map((key) => {
            return action.payload[key];
          });

          return {
            ...item,
            data: [...dataArray, data],
          };
        }

        return item;
      });

      const newTablesStr = JSON.stringify(newTables);

      localStorage.setItem('tables', newTablesStr);

      return {
        ...state, tables: newTables,
      };
    }
    default:
      return state;
  }
};

export const TablesContextProvider = (props:any) => {
  const tablesJSON = localStorage.getItem('tables') || '[]';
  const tablesData = JSON.parse(tablesJSON);

  const [tables, setTable] = useReducer(reducer, { tables: tablesData });

  return (
    <TablesContext.Provider value={{ tables, setTable }}>
      {props.children}
    </TablesContext.Provider>
  );
};
