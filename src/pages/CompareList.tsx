import React, { useContext, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { EditOutlined } from '@ant-design/icons';
import { debounce } from 'lodash';

import { CommonLayout } from '../Layout/CommonLayout';
import { CreateTableButton } from '../components/common/Buttons/CreateTableButton';
import { Input } from '../components/forms/elements/Input/Input';

import { ITableFullData } from '../types';
import { TablesContext } from '../context/TablesContext';
import { ROUTES } from '../consts/Routes';

import styles from '../Layout/Menu/styles.module.scss';

export const CompareList: React.FC = () => {
  const { tables: { tables } } = useContext(TablesContext);
  const [filteredTables, setTables] = useState<ITableFullData[]>(tables);

  const onChange = debounce((value: string) => {
    if (value) {
      const news = tables.filter((table: ITableFullData) => table.tableName.toLowerCase().includes(value.toLowerCase()));
      setTables(news);
    } else {
      setTables(tables);
    }
  }, 300);

  return (
    <CommonLayout>
      <div>
        <Input
          placeholder="Search"
          onChange={(event: React.ChangeEvent<HTMLInputElement>) => onChange(event.target.value)}
        />
      </div>
      {filteredTables.length < 1 ? (
        <span>У вас еще нет ни одной таблицы</span>
      ) : filteredTables?.map((table: ITableFullData, index: number) => (
        <>
          <div className={styles.link} key={table.tableName}>
            <NavLink to={`${ROUTES.COMPARE}/${index}`} className={styles.menuItem}>
              <span className={styles.itemTitle}>{table.tableName}</span>
            </NavLink>
            <NavLink to={`${ROUTES.TABLE}/${index}`} className={styles.menuItem}>
              <span className={styles.pencil}><EditOutlined /></span>
            </NavLink>
          </div>
        </>
      ))}
      <CreateTableButton />
    </CommonLayout>
  );
};
