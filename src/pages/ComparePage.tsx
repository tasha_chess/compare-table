import React, { useContext, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { Button } from 'antd';
import { EditOutlined } from '@ant-design/icons';

import { convertToLatin } from 'src/utils';
import { TablesContext } from '../context/TablesContext';

import { CommonLayout } from '../Layout/CommonLayout';
import { Table } from '../components/table';
import { Modal } from '../components/common/Modal/Modal';
import { AddNewRow } from '../components/forms/AddNewRow/AddNewRow';

import { ROUTES } from '../consts/Routes';
import { IColumnInfo, ColumnType } from '../types';

import styles from '../components/table/styles.module.scss';

export const ComparePage = (props:any) => {
  const { tables: { tables } } = useContext(TablesContext);
  const [isModalVisible, setModalVisible] = useState(false);
  const { id } = props.match.params;
  const table = tables[id];
  const { columns, tableName, data } = table;
  const columnNames: IColumnInfo[] = [];

  const columnsStr = columns && columns.map(({ name }: ColumnType, index:number) => {
    const latinStr = convertToLatin(name).replace(/\s/g, '');
    columnNames.push({ title: name, name: latinStr });

    return {
      title: name,
      key: index,
      dataIndex: index,
      defaultSortOrder: 'descend',
      sorter: (a: [], b: []) => a[index] - b[index],
      render: (cellData: string | number) => (
        <div>{cellData}</div>
      ),
    };
  });

  const addNewRow = () => {
    setModalVisible(true);
  };
  const closeModal = () => {
    setModalVisible(false);
  };

  return (
    <CommonLayout>
      <div className={styles.title}>
        <NavLink to={`${ROUTES.TABLE}/${id}`}>
          <span className={styles.pencil}><EditOutlined /></span>
        </NavLink>
        <span className={styles.tableName}>{tableName}</span>
        <Button type="dashed" onClick={addNewRow}>Add new row</Button>
      </div>
      <Table columns={columnsStr} dataSourse={data} />
      <Modal
        visible={isModalVisible}
        title="Add new row"
        onCancel={closeModal}
        footer={() => {}}
      >
        <AddNewRow columnNames={columnNames} columns={columns} id={id} closeModal={closeModal} />
      </Modal>
    </CommonLayout>
  );
};
