import React from 'react';

import { CommonLayout } from '../Layout/CommonLayout';
import { TableForm } from '../components/forms/TableForm/TableForm';

export const CreateTablePage = (props:any) => (
  <CommonLayout>
    <TableForm id={props.match.params.id} />
  </CommonLayout>
);
