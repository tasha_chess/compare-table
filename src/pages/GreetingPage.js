import React from 'react';
import { NavLink } from 'react-router-dom';

import { GreetingLayout } from '../Layout/GreetingLayout';
import { PageTitle } from '../components/common/PageTitle';

import {
  GREETING, GREETING_TEXT, continueWithoutAuth, toAuth,
} from '../consts/userDialogs';
import { ROUTES } from '../consts/Routes';

import styles from './styles.module.scss';

export const GreetingPage = () => (
  <GreetingLayout>
    <div className={styles.greetingPage}>
      <PageTitle textAlign="center">
        {GREETING}
      </PageTitle>
      <div className={styles.greetText}>{GREETING_TEXT}</div>
      <div className={styles.links}>
        <NavLink to={ROUTES.COMPARE}>{continueWithoutAuth}</NavLink>
        <NavLink to={ROUTES.SIGN_IN}>{toAuth}</NavLink>
      </div>
    </div>
  </GreetingLayout>
);
