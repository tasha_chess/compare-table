export type ColumnType = {
  name: string,
  key: string,
};

export interface IColumnInfo {
  title: string,
  name: string
}

export interface IAction {
  type: string,
  payload: ITableFullData,
}

type DataType = {};

export interface ITableFullData {
  tableName: string;
  columns?: Array<ColumnType>;
  data?: Array<DataType>;
}
