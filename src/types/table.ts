type ColumnType = {
  name: string,
  key: string,
};

export interface ITableAction {
  type: string,
  payload: ITableFullData | any,
  id: string,
}

export interface ITableFullData {
  tableName: string;
  columns?: Array<ColumnType>;
  data?: Array<any>;
}
